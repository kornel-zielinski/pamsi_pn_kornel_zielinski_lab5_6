#include <cstdlib>
#include <iostream>
#include <ctime>
#include <cmath>
#include <windows.h>

template<class type>
void Swap(type* tab, int a, int b) //Zamienia element o indeksie a z elementem b
{
  type tmp=tab[a];
  tab[a]=tab[b];
  tab[b]=tmp;   
}

template<class type>
void SwapTab(type* tab, int len)
{
  for(int i=0;i<len/2;i++)
    Swap(tab, tab[i], tab[len-i-1]);
}

template<class type>
bool IsSorted(type* tab, int len)
{
  for(int i=0; i<len-1; i++)
    if(tab[i]>tab[i+1])
      return false;
  return true;
}

template<class type>
void Merge(type* tab, int b, int q, int e, type* tmp)
{
  int i = b;
  int j = q;
  for(int k=b; k < e; k++)
  {
    if((tab[i] <= tab[j] || j >= e) && i < q)
    {
	  tmp[k]=tab[i];
      i++;
    }
    else
    {
	  tmp[k]=tab[j];
      j++;
    }
  }
}

//[b]eginning, [e]nd
template<class type>
void MergeSort(type* tab, int b, int e, type* tmp)
{
  if(e-b >= 2)
  {
    int q=(b+e)/2;
    MergeSort(tab, b, q, tmp);
    MergeSort(tab, q, e, tmp);
    Merge(tab, b,  q, e, tmp);
    for(int i=b;i<e;i++)
      tab[i]=tmp[i];
  }
}

LARGE_INTEGER startTimer()
{
LARGE_INTEGER start;
DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
QueryPerformanceCounter(&start);
 SetThreadAffinityMask(GetCurrentThread(), oldmask);
 return start;
}

LARGE_INTEGER endTimer()
{
  LARGE_INTEGER stop;
  DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
  QueryPerformanceCounter(&stop);
  SetThreadAffinityMask(GetCurrentThread(), oldmask);
  return stop;
}


int main()
{
  int size[] = {10000, 50000, 100000, 500000, 1000000};
  float percentage[] = {0, 0.25, 0.5, 0.75, 0.95, 0.99, 0.997};
  int* tab, *tmp;
  
  for(int i=0; i<7; i++)   //procent posortowane
  {
    for(int j=0; j<5; j++)   //kazdy rozmiar 
    {
      LARGE_INTEGER start, end;
      int sum=0;
      for(int k=0; k<100; k++) //100 kazdego zestawu
      {
        srand(time(NULL));
        tab = new int[size[j]];
        tmp = new int[size[j]];
        for(int l=0; l<size[j]; l++)
          tab[l] = rand()%size[j];
        MergeSort(tab, 0, (int)(percentage[i]*size[j]), tmp);
        start=startTimer();
        MergeSort(tab, 0, size[j], tmp);
        end=endTimer();
        if(!IsSorted(tab, size[j]))
          std::cerr << "Tablica nie zosta�a poprawnie posortowana!\n";
        delete[] tab;
        delete[] tmp;
        sum=sum+(end.QuadPart-start.QuadPart);
      }   
      std::cout << "Rozmiar: " << size[j] << ' ' << ", posortowanych: "        
                << percentage[i]*100 << "%\n" << sum/100 << "\n\n";
    }           
  }
  for(int j=0; j<5; j++)   //kazdy rozmiar 
    {
      LARGE_INTEGER start, end;
      int sum=0;
      for(int k=0; k<100; k++) //100 kazdego zestawu
      {
        srand(time(NULL));
        tab = new int[size[j]];
        tmp = new int[size[j]];
        for(int l=0; l<size[j]; l++)
          tab[l] = rand()%size[j];
        MergeSort(tab, 0, size[j], tmp);
        SwapTab(tab, size[j]);
        start=startTimer();
        MergeSort(tab, 0, size[j], tmp);
        end=endTimer();
        if(!IsSorted(tab, size[j]))
          std::cerr << "Tablica nie zosta�a poprawnie posortowana!\n";
        delete[] tab;
        delete[] tmp;
        sum=sum+(end.QuadPart-start.QuadPart);
      }   
      std::cout << "Rozmiar: " << size[j] << ' ' << ", posortowanych od tylu: "        
                << 100 << "%\n" << sum/100 << "\n\n";
    }  
  std::cout << "Done.";
  std::cin >> size[1];
}
